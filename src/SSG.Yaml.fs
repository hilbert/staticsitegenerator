﻿namespace SSG

open System
open System.IO
open System.Text.RegularExpressions
open System.Linq
open System.Collections.Generic

open SSG.Model
open YamlDotNet.RepresentationModel

module Yaml = 

    (* Yaml files handling *)
    let LoadYamlText (text:string) = 
        if String.IsNullOrEmpty(text) then
            null
        else
            let yamlStream = new YamlStream();
            use sr = new StringReader(text)
            yamlStream.Load(sr)
            (yamlStream.Documents.[0].RootNode) :?> YamlMappingNode

    let LoadYamlFile filename = 
        if (File.Exists filename) then
            let yamlStream = new YamlStream();
            use sr = new StreamReader(filename)
            yamlStream.Load(sr)
            if (yamlStream.Documents.Count > 0) then (yamlStream.Documents.[0].RootNode) :?> YamlMappingNode else null
        else
            null

    let SplitYamlAndMarkup text : string * string = 
        let regex = new Regex("\n#{5,}\s*", RegexOptions.Multiline)
        let m = regex.Match(text)
        if m.Success then
            let yaml = text.Substring(0, m.Index+1).Trim()
            let articleText = text.Substring( m.Index + m.Length, text.Length - (m.Index + m.Length))
            (yaml, articleText)
        else
            (String.Empty, text)

    (* Recursively load default.yaml files *)
    let rec LoadYamlFiles (rootFolder:DirectoryInfo) (articleFolder:DirectoryInfo) : YamlMappingNode list = 
        (LoadYamlFile (Path.Combine(articleFolder.FullName, "default.yaml"))) ::
            if rootFolder.FullName = articleFolder.FullName then
                []
            else
                LoadYamlFiles rootFolder (articleFolder.Parent)

    (* Enrich Yaml Meta Information with recursively loaded yaml files *)
    let EnrichMetaInfoSourceWithYamlFiles (metaInfoSource:MetaInfoSource) (settings:Settings) : MetaInfoSource =
        let yamlFile = metaInfoSource.ArticleFile.FullName.Substring(0, metaInfoSource.ArticleFile.FullName.Length - metaInfoSource.ArticleFile.Extension.Length) + ".yaml"
        let yamls = Seq.append metaInfoSource.Yamls (LoadYamlFile(yamlFile) :: (LoadYamlFiles (DirectoryInfo settings.InputPath) (metaInfoSource.ArticleFile.Directory)))
        {
            ArticleFile = metaInfoSource.ArticleFile
            Yamls = yamls.Where( fun y -> y <> null).ToArray()
        }

    (* Extract meta information *)
    (* Find key in a single yaml mode *)
    let GetValueFromYaml (ymn:YamlMappingNode) (key : string) : string option = 
        match ymn.Children 
            |> Seq.cast<KeyValuePair<YamlNode, YamlNode>> 
            |> Seq.tryFind (fun kvp -> kvp.Key.ToString().ToLowerInvariant() = key) with
        | Some kvp -> Some (kvp.Value.ToString())
        | None -> None

    (* Find key in a sequence of Yaml documents, if not found return default key *)
    let GetValueFromYamls (yamls : seq<YamlMappingNode>) (key : string) (defaultValue : string) : string =
        match yamls 
            |> Seq.map (fun y -> GetValueFromYaml y key)
            |> Seq.tryFind (fun v -> match v with | None -> false | Some s -> true) with
        | Some(Some value) -> value
        | _ -> defaultValue

