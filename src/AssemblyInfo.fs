﻿module Engine

#light
 
open System.Reflection;
open System.Runtime.CompilerServices;
open System.Runtime.InteropServices;
 
// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[<assembly: AssemblyTitle("StaticSiteGenerator")>]
[<assembly: AssemblyDescription("Static site generator using razor, markdown, textile, creole, yaml")>]
[<assembly: AssemblyConfiguration("")>]
[<assembly: AssemblyCompany("SSG")>]
[<assembly: AssemblyProduct("StaticSiteGenerator")>]
[<assembly: AssemblyCopyright("Noone")>]
[<assembly: AssemblyTrademark("Noone")>]
[<assembly: AssemblyCulture("")>]
 
// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[<assembly: ComVisible(false)>]
 
// The following GUID is for the ID of the typelib if this project is exposed to COM
[<assembly: Guid("08D7AEB2-2E82-41C3-A1E9-5B7110833AA1")>]
 
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the ‘*’ as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[<assembly: AssemblyVersion("1.0.10.0")>]
[<assembly: AssemblyFileVersion("1.0.10.0")>]
()