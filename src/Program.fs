﻿namespace SSG

open System
open System.IO
open System.Globalization
open System.Linq
open System.Net
open System.Reactive.Linq
open System.Reflection
open System.Security
open System.Security.Permissions
open System.Security.Policy
open System.Threading
open SSG.Model
open Microsoft.FSharp.Collections
open Suave
open Suave.Files
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.Http
open Suave.Logging
open Suave.Web
open Suave.Utils
open FSharp.Collections.ParallelSeq

(* File recursion generic - use for touching dll + for finding markdown *)
(* Recursively find all dll's and touch'em *)
(* Use argu for argument parsing *)
(* Try fsUnit *)
(* Make core logic work more elegantly *)
(* Make external functions *)

module Program = 

    (* Parsing of parameters *)
    let GetParamBool param (argv : seq<string>) = (Seq.tryFind (fun (p:string) -> ("-" + param).StartsWith(p)) argv).IsSome

    let rec GetParamString param argv = 
        match argv with
        | [] -> None
        | [s] -> None
        | (p:string)::(v:string)::xs -> if ("-" + param).StartsWith(p) && not (v.StartsWith("-")) then Some(v) else (GetParamString param (v::xs))

    let GetOptional param fallbackValue =
        match param with
        | None -> fallbackValue
        | Some(value) -> value

    (* Loading of settings *)
    let LoadSettings argv = 
        {
            InputPath = FileInfo(GetOptional (GetParamString "input" argv) ".").FullName
            OutputPath = FileInfo(GetOptional (GetParamString "output" argv) ".").FullName
            DefaultPageName = GetOptional (GetParamString "defaultpage" argv) "index.html"
            UrlPathPrefix = GetOptional (GetParamString "prefix" argv) "/"
            DestinationExtension = GetOptional (GetParamString "extension" argv) "html"
            MonitorChange = GetParamBool "monitor" argv
            ServeHttp = GetParamBool "serve" argv
            Port = UInt16.Parse (GetOptional (GetParamString "port" argv) "80")
            MarkupLanguages = [CommonMark; Creole; Markdown; Textile]
        } : Settings

    let SetupEventNotification (settings:Settings) =
        let watcher = new FileSystemWatcher(settings.InputPath)
        watcher.EnableRaisingEvents <- true
        watcher.IncludeSubdirectories <- true
        let observerableFilesystem = Observable.merge watcher.Changed (Observable.merge watcher.Created watcher.Deleted)
        observerableFilesystem
            .Throttle(TimeSpan.FromMilliseconds(500.0))
            .Subscribe(fun fileSystemEventArgs -> Engine.RunTransformations settings)

    (* Touch local assemblies, making them available in razor templates *)
    let TouchDllByFilename filename = 
            try
                let asm = Assembly.LoadFrom(filename)
                Utils.Log(String.Format("Touched:{0}", asm.FullName))
            with
            | ex -> ()

    let TouchDllByAssemblyName (fullname:AssemblyName) = 
            try
                let asm = Assembly.Load(fullname)
                Utils.Log(String.Format("Touched:{0}", asm.FullName))
            with
            | ex -> ()

    (* Touch dlls in directory of the executable & directory of the caller *)
    let TouchDllsInFolders() =
        [| AppDomain.CurrentDomain.BaseDirectory; Directory.GetCurrentDirectory() |] 
        |> Seq.map (fun d -> DirectoryInfo(d).GetFiles("*.dll") )
        |> Seq.concat
        |> Seq.map (fun fi -> fi.FullName)
        |> Seq.distinct
        |> PSeq.iter TouchDllByFilename

    (* Touch referenced assemblies *)
    let TouchReferencedDlls() = 
        let knownAssemblies = AppDomain.CurrentDomain.GetAssemblies()
        let notLoadedAssemblyNames = 
            knownAssemblies 
            |> Seq.map (fun a -> a.GetReferencedAssemblies()) 
            |> Seq.concat 
            |> Seq.distinctBy (fun a -> a.FullName)
            |> Seq.where (fun refAssemblyName -> not(knownAssemblies.Any( fun ka -> ka.FullName = refAssemblyName.FullName)))
            |> Seq.toArray
        if notLoadedAssemblyNames.Any() then
            PSeq.iter TouchDllByAssemblyName notLoadedAssemblyNames

    let TouchAssemblies() = 
        TouchDllsInFolders()
        TouchReferencedDlls()

    (* Start suave web server *)
    let startWeb settings (cancellationTokenSource:CancellationTokenSource) = 
        let config = { defaultConfig with 
                        homeFolder = Some(settings.OutputPath); 
                        bindings = [ HttpBinding.create HTTP IPAddress.Any settings.Port ]; 
                        //logger = Loggers.ConsoleWindowLogger LogLevel.Verbose;
                        cancellationToken = cancellationTokenSource.Token }

        let errorHandler     = RequestErrors.NOT_FOUND "404 Not Found"
        let indexPageHandler = pathRegex ".*/$" >=> request(fun r -> file (settings.OutputPath + r.url.AbsolutePath.Replace("/", "\\") + settings.DefaultPageName))
        let getHandler       = Filters.GET >=> choose [ browseHome; indexPageHandler; dirHome]
        choose [ getHandler; errorHandler ] 
        |> startWebServerAsync config
        |> snd
        |> Async.StartAsTask
        |> ignore

    [<EntryPoint>]
    let main argv = 
        let settings = LoadSettings (Array.toList(argv))

        TouchAssemblies()
        if settings.MonitorChange then
            // Utils.Log("Starting initial transformation...")
            Engine.RunTransformations settings
            Utils.Log("Monitoring folder: " + settings.InputPath)
            use notification = SetupEventNotification settings
            let cancellationTokenSource = new CancellationTokenSource()
            startWeb settings cancellationTokenSource
            Utils.Log("Press any key to stop...")
            ignore (Console.ReadKey())
            Utils.Log("Stopping...")
            cancellationTokenSource.Cancel()
        else
            Utils.Log("Compiling folder: " + settings.InputPath)
            Engine.RunTransformations settings
        0
