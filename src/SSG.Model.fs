﻿namespace SSG

open System
open System.IO
open System.Text.RegularExpressions
open YamlDotNet.RepresentationModel

module Model = 
    type Language = CommonMark | Creole | Markdown | Textile | ReStructuredText | WikiMedia | No
    type MarkupDefinition = { Language : Language; Extensions : string list }
    type MarkupText = { Language: Language; Text : string }

    type MetaInfoSource = { 
        ArticleFile: FileInfo 
        Yamls: YamlMappingNode[]
    }

    type MetaInfo = {
        
        (* \Documentation\Connectivity.md *)
        RelativeFilePath:string
        
        (* Documentation/Connectivity.html *)
        RelativeUri: string

        (* Documentation/ *)
        RelativeDirectoryPath:string
        
        (* /Documentation/Connectivity.html *)
        AbsoluteUri: string

        AbsoluteDirectoryPath: string

        (* C:\hilbert\src\staticsitegenerator\Scimore\output\Documentation\Connectivity.html *)
        DestinationFullPath: string
        
        (* Connectivity *)
        Title: string
        LastModified: DateTime
        Written: DateTime
        RazorTemplateFilePath: string
        Author: string
        Summary: string
        Tags: string[]
    }

    type Article = {
        Markup: MarkupText
        MetaInfoSource: MetaInfoSource
        MetaInfo: MetaInfo
        TransformedMarkup: string
    }
    
    type PageModel = {
        Article: Article
        Articles: Article[]
    }

    (* Internal types used during processing *)
    type Settings = {
        InputPath:string
        OutputPath:string
        DestinationExtension: string
        UrlPathPrefix: string
        MarkupLanguages: Language list
        DefaultPageName: string

        MonitorChange: bool

        ServeHttp: bool
        Port: UInt16
    }

    type MarkupWithMetaInfo = {
        Markup: MarkupText
        MetaInfoSource: MetaInfoSource
        MetaInfo: MetaInfo
    }

    (* Get the distinct list of relative paths *)
    let DistinctRelativePaths (pageModel: PageModel) : string seq =
        pageModel.Articles |> Seq.map (fun a -> a.MetaInfo.RelativeDirectoryPath) |> Seq.distinct |> Seq.sortBy (fun p -> p.Trim())

    (* Get the list of articles with a specific relative path *)
    let ArticlesWithRelativeDirectoryPath (pageModel: PageModel) (relativeDirectoryPath:string) : Article seq = 
        pageModel.Articles |> Seq.where (fun a -> a.MetaInfo.RelativeDirectoryPath = relativeDirectoryPath) |> Seq.sortBy (fun a -> a.MetaInfo.Title.Trim())

    (* Bread crumb list *)
    let rec BreadCrumbParentArticles prefix arr = 
        match arr with
        | [] -> []
        | _::[] -> []
        | x::xs -> (prefix + x + "/")::(BreadCrumbParentArticles ((prefix + x + "/")) xs)

    let FindArticleWithRelativePath articles relativePath : Article option =
        articles |> Seq.tryFind (fun a -> a.MetaInfo.RelativeUri = relativePath)

    let BreadCrumbList (model:PageModel) : Article list =
        model.Article.MetaInfo.RelativeUri.Split([|'/'|]) 
            |> Seq.filter (fun s -> not(String.IsNullOrEmpty(s))) 
            |> Seq.toList
            |> BreadCrumbParentArticles ""
            |> List.choose (FindArticleWithRelativePath model.Articles)

    (* Generate summary *)
    let regexRemoveHeadlines = new Regex("\\<([h|H]\\d)(.*?)\\>(.*?)\\</\\1\\>", RegexOptions.Multiline ||| RegexOptions.CultureInvariant ||| RegexOptions.Compiled)
    let regexRemoveMultiSpaces = new Regex("\\s+");
    let regexRemoveTags = new Regex("\\<(.*?)\\>", RegexOptions.IgnoreCase ||| RegexOptions.CultureInvariant ||| RegexOptions.Compiled)
    let Summary (maxLength:int, article:Article) : string = 
        let s = if not(String.IsNullOrEmpty(article.MetaInfo.Summary)) then 
                    article.MetaInfo.Summary
                else 
                    regexRemoveMultiSpaces.Replace(regexRemoveTags.Replace(regexRemoveHeadlines.Replace(article.TransformedMarkup, " "), " "), " ").Trim()
        if s.Length <= maxLength then
            s
        else
            s.Substring(0, s.LastIndexOf(' ', maxLength)) + "..."

    (* Get the uri for a specific page - for getting page links inside razor templates *)
    let GetLink (model:PageModel) (title:string) : string = 
        match model.Articles |> Seq.tryFind (fun a -> (a.MetaInfo.Title = title)) with 
        | None -> String.Empty
        | Some(a) -> a.MetaInfo.AbsoluteUri
