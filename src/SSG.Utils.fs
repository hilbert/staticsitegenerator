﻿namespace SSG

open System
open System.Threading

(* Console writelines guarded using lock to ensure  *)
(* nice console output from concurrent threads      *)
module Utils =
    let lockobj = new System.Object()

    let Log (s: string) = 
        Monitor.Enter lockobj
        try
            Console.WriteLine(s)
        finally
            Monitor.Exit lockobj
