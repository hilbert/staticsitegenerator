﻿namespace SSG

open System
open System.IO
open System.Linq
open System.Collections.Generic
open System.Net
open System.Text.RegularExpressions
open SSG.Model
// open Textile
open YamlDotNet.RepresentationModel

module Markup = 
    let MarkupDefinitions = [ 
        { Language = CommonMark; Extensions = ["md"; "cm"; "commonmark"] };
        { Language = Creole; Extensions = ["creole"] };
        { Language = Markdown; Extensions = ["markdown"] };
        { Language = Textile; Extensions = ["textile"] }
    ]

    let GetMarkupExtensions (languages: Language list) = 
        MarkupDefinitions |> List.choose (fun d -> if languages.Contains(d.Language) then Some(d.Extensions) else None) |> List.concat

    let GetLanguage (extension: string) : Language option = 
        MarkupDefinitions |> List.filter (fun d -> d.Extensions.Contains(extension.ToLowerInvariant())) |> List.map (fun d -> d.Language) |> List.tryFind (fun _ -> true)

    let MarkdownLink (article:MarkupWithMetaInfo) = 
        // [foo]: http://example.com/  "Optional Title Here"
         Environment.NewLine + "[" + article.MetaInfo.Title + "]: " + article.MetaInfo.AbsoluteUri.Replace(" ", "+") + " \"" + article.MetaInfo.Title + "\""

    let MarkdownLinks (articles: MarkupWithMetaInfo list) =
        articles
        |> Seq.map MarkdownLink 
        |> String.Concat

    //let CreoleLinkCallback (articles: MarkupWithMetaInfo list) (e : Wiki.LinkEventArgs) : unit = 
    //    match articles |> List.tryFind (fun a -> e.Link = a.MetaInfo.Title) with
    //    | None -> ()
    //    | Some(article) -> 
    //        e.Href <- article.MetaInfo.AbsoluteUri
    //        e.Text <- article.MetaInfo.Title
    //        e.Target <- Wiki.LinkEventArgs.TargetEnum.Internal
    //    ()

    // http://www.toptensoftware.com/Articles/83/MarkdownDeep-Syntax-Highlighting
    let MarkdownDeepFormatCodePrettyPrint (m : MarkdownDeep.Markdown) (code:string) : string =
        let m = Regex.Match(code, "^({{(.+)}}[\\r\\n])")
        if (m.Success) then
            let language = m.Groups.[2].ToString()
            let remainingcode = code.Substring(m.Groups.[1].Length)
            let s = String.Format("<pre><code language=\"{0}\">{1} </code ></pre >\r\n", language, remainingcode.Trim());
            s
        else
            String.Format("<pre><code>{0} </code></pre>\r\n", code.Trim());

    let Transform (article : MarkupWithMetaInfo) (articles: MarkupWithMetaInfo list) : string =
        match article.Markup.Language with
        | Language.CommonMark ->
            let markup = article.Markup.Text + Environment.NewLine + (MarkdownLinks articles)
            // For debug purposes enable the below line
            // File.WriteAllText(article.MetaInfo.DestinationFullPath + ".commonmark", markup)
            CommonMark.CommonMarkConverter.Convert(markup)
        | Language.Markdown ->
            // https://github.com/toptensoftware/markdowndeep/
            let markup = article.Markup.Text + (MarkdownLinks articles)
            // For debug purposes enable the below line
            // File.WriteAllText(article.MetaInfo.DestinationFullPath + ".markdown", markup)
            let deep = new MarkdownDeep.Markdown()
            deep.SafeMode <- false
            deep.ExtraMode <- true
            deep.MarkdownInHtml <- true
            deep.AutoHeadingIDs <- true
            deep.FormatCodeBlock <- new Func<MarkdownDeep.Markdown,String,String>(MarkdownDeepFormatCodePrettyPrint)
            deep.Transform(markup)
        //| Language.Creole -> 
        //    let creole = Wiki.CreoleParser()
        //    creole.OnLink.Add( CreoleLinkCallback articles)
        //    creole.ToHTML(article.Markup.Text)
        //| Language.Textile -> 
        //    let sbf = new StringBuilderOutputter()
        //    let textfileFormatter = new TextileFormatter(sbf)
        //    textfileFormatter.Format(article.Markup.Text)
        //    sbf.GetFormattedText()
        | _ -> article.Markup.Text