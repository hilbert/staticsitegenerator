﻿namespace SSG

open System
open System.IO
open System.Linq
open SSG.Model
open SSG.Yaml
open RazorEngine.Configuration
open RazorEngine.Templating
open Microsoft.FSharp.Collections
open FSharp.Collections.ParallelSeq

module Engine = 

    (* Recursively find articles *)
    let FindFilesByExtensions (patterns:seq<string>) (path:string) : seq<string> =
        patterns 
        |> Seq.map (fun ext -> Directory.EnumerateFiles(path, "*."+ext))
        |> Seq.concat

    let rec RecurseDirectories path : seq<string> = 
        Directory.EnumerateDirectories(path)
        |> Seq.map RecurseDirectories
        |> Seq.concat
        |> Seq.append [path]

    let FindArticles path extensions =
       RecurseDirectories path
       |> Seq.map (FindFilesByExtensions extensions)
       |> Seq.concat

    (* Extract all meta information given the MetaInfoSource and Settings *)
    let ExtractMetaInfoFromMetaInfoSource (metaInfoSource:MetaInfoSource) (settings:Settings) : MetaInfo =
        let defaultPageName = GetValueFromYamls metaInfoSource.Yamls "defaultpagename" settings.DefaultPageName
        let destinationExtension = GetValueFromYamls metaInfoSource.Yamls "destinationextension" settings.DestinationExtension
        let f = metaInfoSource.ArticleFile
        let relativeFilePath = f.FullName.Replace(settings.InputPath, "").Substring(1)
        let relativeUri = (relativeFilePath.Substring(0, relativeFilePath.LastIndexOf('.')+1).Replace("\\", "/") + destinationExtension).Replace(defaultPageName, "")
        let relativeDirectoryPath = (f.DirectoryName + "\\").Replace(settings.InputPath + "\\", "").Replace("\\", "/")
        let absoluteUri = (settings.UrlPathPrefix + relativeUri).Replace(defaultPageName, "")
        let absoluteDirectoryPath = settings.UrlPathPrefix + relativeDirectoryPath

        let srcfilenameWithoutExtension = f.FullName.Substring(0, f.FullName.Length - f.Extension.Length)
        let destinationFullPath = (srcfilenameWithoutExtension + "." + destinationExtension).Replace(settings.InputPath, settings.OutputPath)
        let filename = srcfilenameWithoutExtension.Replace(f.DirectoryName + "\\", "")
        let razorPath = GetValueFromYamls metaInfoSource.Yamls "razor" String.Empty
        {
            RelativeFilePath = relativeFilePath
            RelativeUri = relativeUri
            RelativeDirectoryPath = relativeDirectoryPath
            AbsoluteUri = absoluteUri
            AbsoluteDirectoryPath = absoluteDirectoryPath
            DestinationFullPath = destinationFullPath
            Title = GetValueFromYamls metaInfoSource.Yamls "title" (filename.Replace("_", " "))
            LastModified = metaInfoSource.ArticleFile.LastWriteTime
            Written = DateTime.Parse(GetValueFromYamls metaInfoSource.Yamls "written" (metaInfoSource.ArticleFile.LastWriteTime.ToString()))
            RazorTemplateFilePath = if not(String.IsNullOrEmpty(razorPath)) then Path.Combine(settings.InputPath, razorPath) else String.Empty
            Author = GetValueFromYamls metaInfoSource.Yamls "author" String.Empty
            Summary = GetValueFromYamls metaInfoSource.Yamls "summary" String.Empty
            Tags = (GetValueFromYamls metaInfoSource.Yamls "tags" String.Empty).Split(',').Where(fun s -> not(String.IsNullOrEmpty(s))).ToArray()
        } : MetaInfo

    (* Load markup *)
    let LoadMarkup filePath : (MarkupText * MetaInfoSource) = 
        let fileInfo = new FileInfo(filePath)
        let fileContent = File.ReadAllText(filePath)
        let language = match Markup.GetLanguage(fileInfo.Extension.Replace(".", "")) with | Some(l) -> l | _ -> Language.Markdown
        let (yaml, article) = SplitYamlAndMarkup fileContent
        (
            { Language = language ; Text = article },
            {
                ArticleFile = fileInfo
                Yamls = [| LoadYamlText yaml |]
            }
        )

    let LoadMarkupAndMetaInfo (settings:Settings) (filepath:string)  =
        let (markupText, fileMetaInfoSource) = LoadMarkup filepath
        let metaInfoSource = EnrichMetaInfoSourceWithYamlFiles fileMetaInfoSource settings
        {
            Markup = markupText
            MetaInfoSource = metaInfoSource
            MetaInfo = ExtractMetaInfoFromMetaInfoSource metaInfoSource settings
        } : MarkupWithMetaInfo

    (* Run markup transformations on article *)
    let TransformMarkup (articles:MarkupWithMetaInfo list) (markupWithmetaInfo: MarkupWithMetaInfo) = 
        {
            Markup = markupWithmetaInfo.Markup
            MetaInfoSource = markupWithmetaInfo.MetaInfoSource
            MetaInfo = markupWithmetaInfo.MetaInfo
            TransformedMarkup = 
                try
                    SSG.Markup.Transform markupWithmetaInfo articles
                with
                    | ex ->
                        Utils.Log(String.Format("ERROR: Transforming article \"{0}\". Error: {1}", markupWithmetaInfo.MetaInfo.RelativeFilePath, ex.Message))
                        String.Empty
        } : Article

    (* Default razor template *)
    let defaultTemplate = "<html><head><title>@Raw(Model.Article.MetaInfo.Title)</title></head><body style=\"margin:20px;\">@Raw(Model.Article.TransformedMarkup)</body></html>"
    let serviceConfiguration = TemplateServiceConfiguration()
    serviceConfiguration.CachingProvider <- new DefaultCachingProvider(fun t -> ())
    let razorService = RazorEngineService.Create(serviceConfiguration)

    (* Razor an article *)
    let ApplyRazorTemplate (pageModel:PageModel) = 
        let razorFile = pageModel.Article.MetaInfo.RazorTemplateFilePath
        Utils.Log(sprintf "Razoring: %s" pageModel.Article.MetaInfo.DestinationFullPath)
        try
            let template = if not(String.IsNullOrEmpty(razorFile)) then File.ReadAllText(razorFile) else defaultTemplate
            razorService.RunCompile(template, Guid.NewGuid().ToString(), typeof<PageModel>, pageModel)
            // Razor.Parse(template, pageModel, razorFile)
        with | ex -> let errorMessage = String.Format("ERROR: Applying razor template \"{0}\" on \"{1}\". Error: {2}", pageModel.Article.MetaInfo.RazorTemplateFilePath, pageModel.Article.MetaInfo.RelativeFilePath, ex.Message)
                     Utils.Log(errorMessage)
                     errorMessage

    (* Run razor template on articles *)
    let TransformRazor (articles: Article[]) (article:Article) : (string * string) =
        let pageModel = { Article = article ; Articles = articles} : PageModel
        (article.MetaInfo.DestinationFullPath, ApplyRazorTemplate pageModel)

    (* Misc functions *)
    let CreateDirAndSaveFile (filename : string, content:string) : unit = 
        try
            let d = FileInfo(filename).Directory
            if not (d.Exists) then d.Create()
            File.WriteAllText( filename, content)
        with | ex -> Utils.Log(String.Format("ERROR: Saving file \"{0}\". Error: {1}", filename, ex.Message))

    (* Run full transformation on given settings *)
    let RunTransformations (settings:Settings) =
        let sw = Diagnostics.Stopwatch.StartNew()
        Utils.Log("Running transformations")

        (* Load markup files *)
        let articles = FindArticles settings.InputPath (Markup.GetMarkupExtensions settings.MarkupLanguages) 
                       |> PSeq.map (LoadMarkupAndMetaInfo settings) 
                       |> PSeq.toList

        (* Run markup transformation *)
        let transformedArticles = (
            articles 
            |> PSeq.map (TransformMarkup articles) 
            |> PSeq.sortBy (fun a -> a.MetaInfo.Title)).ToArray()

        (* Run razor transformation *)
        transformedArticles 
            |> PSeq.map (TransformRazor (transformedArticles))  
            |> PSeq.iter CreateDirAndSaveFile 

        Utils.Log(String.Format("Completed transformations in {0}ms", sw.Elapsed.TotalMilliseconds))
