# Meta information

Meta information are information related to the articles. SSG pre-defines a set of easy accessible meta information, but also allows for arbitrary information being attached to each article/document, or groups of articles/documents.

The meta information is available to the razor templates, allowing rich content on the site.

Examples of such are: title, tags, author, created date, content source, razor template.


## Strongly typed meta information

To make life easier when doing templates, SSG have predefined meta structure information containing the following fields:

    type MetaInfo = {
        RelativeFilePath:string
        RelativeUri: string
        RelativeDirectoryPath:string
        AbsoluteUri: string
        AbsoluteDirectoryPath: string
        DestinationFullPath: string
        Title: string
        LastModified: DateTime
        RazorTemplateFilePath: string
        Author: string
        Summary: string
        Tags: string[]
    }

Accessing this information from the razor template is simple. Here an example on how the title of this page is inserted through the razor template:

	<title>@Model.Article.MetaInfo.Title</title>	

## Source of meta information

The meta information comes from two sources.

1) The file information for the article/document

2) [Yaml](http://en.wikipedia.org/wiki/YAML) markup.


## Specifying Meta information in Yaml

Inspired by [Jekyll](http://jekyllrb.com/), SSG uses Yaml for defining arbitrary meta information.
The meta information can be spedified in three different ways.

### 1) Yaml in the header of the markup file.

For example if you want to overwrite the default title, taken form the document/article filename, you can in the header of the document define the title using a yaml block. Example:

	title: This is the new header
	#####
	This is the article body. Lorem ipsum...

It is also possible to overwrite which razor template should be used for a specific page. In this example site the article/document "ListAllArticles.md" uses this trick to overwrite the default razor template:

	razor: templates\ListAllArticles.cshtml
	#########################
	# List of all articles....

### 2) Seperate yaml file pr. article/document

Given you have an article/document file named "MetaInformation.md", SSG will look for a yaml file named "MetaInformation.yaml".


### 3) Seperate default.yaml files in the folders of the site

Recursively SSG will search for default.yaml files in the input folders of your site. This allows for parts of the sites run with different values. E.g. in the root of this example site you will find the following "default.yaml" file:

    razor: templates\default.cshtml

This says that if no other razor template is specified either 1) in the header of the markup file, 2) or seperate document yaml files, 3) or a subfolders "default.yaml" - all articles/documents will always fall back to using the "templates\default.cshtml" razor template.

## Accessing the meta information in razor templates

The meta information is available through the [page model][ShowAllArticleAndMetaInformationOnArticle].
