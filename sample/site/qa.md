# Questions'n'Answers

### Why was the software developed?

I needed a static site generator and I wanted to dust of my very ancient sml/ml skills by trying out f#. 

### Will you support the software

As long as I use the software, I will. Its open source, so you can support your self.

### Who is using the software

Me

### Why isn't markup language XXX is supported.

I just implemented a couple of markup languages that I needed, and for which there are .net library to transform markup into html.
If you have another markup language, and a .net library supporting the transformation to html, contact me.

### I need a feature

Cool, make it and send me the change, and I will consider merging it.

### Your code sucks

Yes I know. Please email me comments, advice & suggestions. This is my first f# project.

### Why no IoC and unit tests

That will be next project. FsUnit looks interesting.

### What is the license

None. Use as you see fit.

### Why bitbucket and not github?

Bitbucket allows me to have private repo's. 

### Is there a tool that can convert between markup languages

Try this one: [http://johnmacfarlane.net/pandoc/](http://johnmacfarlane.net/pandoc/).

### The http server refuses to start.

Check with "netstat -a -b" which process is stealing port 80. Skype has a nasty tendency to do so.

### How do I contact you?

UUDecode the following string:

	1=&AO;6%S0&AI;&)E<G0N9&L`
