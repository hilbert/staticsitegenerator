razor: templates\ShowAllArticleAndMetaInformationOnArticle.cshtml
author: Thomas
#########################

# The page model

The model given to razor templates. It contains:

* The markup and the transformed markup of the article.

* A list of all articles. This gives each article "page" full access to all data for the site.

* Set of meta information for the article, and all sources for meta information

## The definition of the model
The definition of the model, here in f# (should be fairly easy to understand):

    type Language = CommonMark | Creole | Markdown | Textile | ReStructuredText | WikiMedia | No
    type MarkupDefinition = { Language : Language; Extensions : string list }
    type MarkupText = { Language: Language; Text : string }

    type MetaInfoSource = { 
        ArticleFile: FileInfo 
        Yamls: YamlMappingNode[]
    }

    type MetaInfo = {
        
        (* \Documentation\Connectivity.md *)
        RelativeFilePath:string
        
        (* Documentation/Connectivity.html *)
        RelativeUri: string

        (* Documentation/ *)
        RelativeDirectoryPath:string
        
        (* /Documentation/Connectivity.html *)
        AbsoluteUri: string

        AbsoluteDirectoryPath: string

        (* C:\hilbert\src\staticsitegenerator\Scimore\output\Documentation\Connectivity.html *)
        DestinationFullPath: string
        
        (* Connectivity *)
        Title: string
        LastModified: DateTime
        RazorTemplateFilePath: string
        Author: string
        Summary: string
        Tags: string[]
    }

    type Article = {
        Markup: MarkupText
        MetaInfoSource: MetaInfoSource
        MetaInfo: MetaInfo
        TransformedMarkup: string
    }
    
    type PageModel = {
        Article: Article
        Articles: Article[]
    }


## Example of model given to generate this page

In the following you can see the information available, when executing a template on this article/document.

The examples are json serialized version of the objects available. They simply generated using the following razor commands:
	
	<pre>@JsonConvert.SerializeObject(Model.Article.Markup, Formatting.Indented)</pre>