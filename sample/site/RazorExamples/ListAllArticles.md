razor: templates\ListAllArticles.cshtml
#########################
# List of all articles
This page show the list of all articles on the the current site. Its generated using a razor template that through the Model iterates over all articles.

The razor template for generating this output is:

    @Raw(Model.Article.TransformedMarkup)
    @foreach(var article in Model.Articles)
    {
    	<p>
            <a href="@article.MetaInfo.AbsoluteUri">@article.MetaInfo.Title</a><br />
            Summary: @SSG.Model.Summary(100, @article)<br />
            Using razor template: "@article.MetaInfo.RazorTemplateFilePath"<br />
            From article: "@article.MetaInfoSource.ArticleFile.FullName"
    	</p>
	}




