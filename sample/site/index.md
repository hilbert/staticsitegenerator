title: Static site generator using razor/markdown/creole/textile
tags: f#, static site generator, html generator, .net, c#, razor, markdown, creole, textile, blog, documentation, wiki
author: thomas hilbert madsen
summary: Static site generator using razor/markdown/creole/textile, written in F#. Open source & free.
lastmodified: 2013-08-31
####################
# StaticSiteGenerator (SSG)

StaticSiteGenerator (SSG) is a "static site generator". Made in [F#](http://fsharp.org/) and open source. It glues different libraries together forming a simple and flexible tool for generating web sites.

It can be used to generate documentation web sites, run blogs, wiki's etc.

SSG generates html files from markup files. It allows different markup languages for defining articles/documents - and allows for post processing/wrapping using the asp.net mvc razor template engine.

Point SSG at a directory and it will:

* Find all [markup] files

* Transform all [markup] files to html.

* Run the [razor template engine][razor] (C#) on the transformed markup.

* Save the resulting files, one for each markup file.


# Features

* Automatic inter-linking between articles. E.g. if you write \[Bingo\] in a markdown article, it will automatically link to the "Bingo" page

* [Meta information][metainformation] on markup articles/documents allowing markup documents having keywords, author, tags etc. The meta info can also be used to control the processing, e.g. choose which razor template should be used for a specific article.

* Allow different markup languages on same site. So far [CommonMark], [Markdown], [Creole] and [Textile].

* Post processing and wrapping of articles using the [asp.net mvc razor](http://en.wikipedia.org/wiki/ASP.NET_Razor_view_engine) template engines. Using the razor engine it's possible "inject code into" your page. E.g. blog features insert top 10 latest articles, create tag clouds, automatically create breadcrumbs, create full sitemaps, or post processing articles for syntax highlighting.

* Build in web server for testing your web site.

* Monitor folder and regenerate site on changes, or simple one off transformation.


# Get started

Here are two different ideas on how to get started...

## Play around with this site

The easiest way is just [download](https://bitbucket.org/hilbert/staticsitegenerator/downloads) the zip file, and start modifying the default template. The default template is this site, exemplifying and documenting the features of SSG.

After downloading the site unzip it in a folder. Start a cmd.exe as admin (needed to start build in web server) and run the following command:

	start-demo.bat

Start a browser and load the following uri [http://localhost/index.html](http://localhost/index.html), and you should see this site.

Now you can start modifying files and see what happens.

## Make your own site

[Download](https://bitbucket.org/hilbert/staticsitegenerator/downloads) and unpack the zip file with SSG - you only need the content of the "bin" folder which contains the SSG binaries.

Create an "input" folder where you will put your articles/markup documents.

Create a few markup documents and then run static site generator:

	StaticSiteGenerator.exe -i <your input folder> -o <output folder>

You should now have generated a static web site. Now you can start playing with applying custom [razor] templates.
You can also continue modifying your files. Then I would suggest you using the command (as administrator):

	StaticSiteGenerator.exe -i <your input folder> -o <output folder> -s -m

Which will start the build in web server and monitor your input folder for changes. When changes are detected, the site is regenerated.

# The inner workings of SSG

Its all about three concepts and a simple generation workflow. The concepts are [markup files][markup], the [meta information][metainformation], and the [razor templates and page model][razor].

And the generation workflow is:

* Locate and load all markup files in the input folder(s)
* For each article, load all meta information
* For each article, convert markup to html
* For each article, combine markup, meta information, and converted markup into a page model
* For each article, give page model to razor script, and save resulting html in the output folder(s)


# Command line parameters

<table>
<tr><td><code>-input {source path}</code></td><td>Input folder, location of markup files</td></tr>
<tr><td><code>-output {destination path}</code></td><td>Output folder. Where to drop generated files</td></tr>
<tr><td><code>-defaultpage {default page name}</code></td><td>Default page name, defaults to index.html</td></tr>
<tr><td><code>-extension {destination file extension}</code></td><td>Default file extension for output files, defaults to html</td></tr>
<tr><td><code>-prefix {absolute-url-prefix}</code></td><td>What to prefix the relative url with. Default value: "/"</td></tr>

<tr><td><code>-monitor</code></td><td>Monitor folder for change, and regerate site on change</td></tr>
<tr><td><code>-serve</code></td><td>Start build in web server</td></tr>
<tr><td><code>-port {port}</code></td><td>Port for default web server. Default value: 80</td></tr>
</table>
	

# License'n'Sources

Use as you see fit. You're welcome to share your changes with me. 

Sources can be found here: [https://bitbucket.org/hilbert/staticsitegenerator/](https://bitbucket.org/hilbert/staticsitegenerator/)


# Shouts

This small piece of software is standing on the shoulders of other libraries:

Razor engine wrapper: [https://github.com/Antaris/RazorEngine](https://github.com/Antaris/RazorEngine)

CommonMark: [https://github.com/Knagis/CommonMark.NET](https://github.com/Knagis/CommonMark.NET)

Markdown: [http://www.toptensoftware.com/markdowndeep/](http://www.toptensoftware.com/markdowndeep/)

Creole: [http://creoleparser.codeplex.com](http://creoleparser.codeplex.com)

Textile: [http://textilenet.codeplex.com](http://textilenet.codeplex.com)

Yaml: [https://github.com/aaubry/YamlDotNet](https://github.com/aaubry/YamlDotNet)

Reactive Extensions: [http://msdn.microsoft.com/en-us/data/gg577609.aspx](http://msdn.microsoft.com/en-us/data/gg577609.aspx)

Suave Web Server: [http://suave.io/](http://suave.io/)

# QA

Further questions check the [QA].