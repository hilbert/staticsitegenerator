# Page created using Markdown

Syntax is described here:
[http://daringfireball.net/projects/markdown/syntax](http://daringfireball.net/projects/markdown/syntax)

The .net library used by SSG is found here:
[http://www.toptensoftware.com/markdowndeep/](http://www.toptensoftware.com/markdowndeep/)

Links to other markup example pages:
[CommonMark], [Creole], [Textile]

