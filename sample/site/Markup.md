# Markup

SSG currently knows of 4 markup languages [CommonMark], [Markdown], [Creole] and [Textile].

SSG will search the input folder recursively for file with extensions .commonmark, .markdown, .md, .creole and .textile files. It is possible to used different markup languages on the same site.

Each markup file is mapped to an output file, and the input folder structure is preserved in the output folders.

For advanced "code" pages, e.g. like a sitemap, a little trick is to have empty markup files containing only a meta information section specifying a razor template.

Embedding images is done by dropping the images manually in the output folder, and refer to them from the markup.
