# Razor templates

The razor templates are wrapping the output from the markup engines. 
The templates have access to all information, through the [page model][ShowAllArticleAndMetaInformationOnArticle]. 
The [page model][ShowAllArticleAndMetaInformationOnArticle] contains the markup and all articles and their [meta information][MetaInformation]. 

An example of a simple razor template:

	<html>
	    <head>
	    	<title>
				@Model.Article.MetaInfo.Title
	    	</title>	
	   	</head>
	    <body>
	        @Raw(Model.Article.TransformedMarkup)
	    </body>
	</html>

Here you see how, through the model, the razor template access the information about the article - in this case the Title and the TransformedMarkup.

On this sample site two more advanced templates are demonstrated (check the code on the example site):

* [List all articles][ListAllArticles]

* [Show all information on available for template for an article][ShowAllArticleAndMetaInformationOnArticle]

## The default razor template

If you do not specify your own template is, SSG falls back to its simple default template:

	<html>
		<head><title>@Raw(Model.Article.MetaInfo.Title)</title></head>
		<body style=\"margin:20px;\">@Raw(Model.Article.TransformedMarkup)</body>
	</html>

## Specifying a custom razor template
You specify razor templates using the [meta information][MetaInformation] system.

If you want a special default template to be used on your site then put a "default.yaml" file in the root of your source folder with the following content:

	razor: templates\default.cshtml


If you want a specific article use a specific razor template you can overwrite the razor template for that article by added a meta information header inside the markup file:

	razor: templates\ListAllArticles.cshtml
	#####
	# List of all articles

Here the ##### is the seperator between the yaml meta header section in the top, and the markup article



# Build in helper functions
To make life simpler, a few helper functions have been added.

## Get link
Get a link to an article from a razor template

	@SSG.Model.GetLink(@Model, "Markdown")

## Generating summaries
There is a build in function to generate summaries of articles. 
	
	@SSG.Model.Summary(100, @Model.Article)

This is usefull for meta headers, for sitemaps etc

## Generating bread crumbs

There is a build in function to generate bread crumbs, following the hierarchy folder structure of the input folder.

    @{
        var articles = SSG.Model.BreadCrumbList(Model);
    }

    @foreach (var article in articles)
    { 
        <span>&raquo; <a href="@article.MetaInfo.AbsoluteUri">@article.MetaInfo.Title</a>
        </span>
    }
    @Raw(articles.Length > 0 ? "<br />" : string.Empty)

# Loading external dll's
If you need external dll's, place them in the folder of the SSG executable, and SSG will load the dll's ensuring they are available for the razor template engine.

