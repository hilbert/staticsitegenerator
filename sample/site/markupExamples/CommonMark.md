# Page created using Markdown

Syntax is described here:
[http://commonmark.org/help/](http://commonmark.org/help/)

The .net library used by SSG is found here:
[https://github.com/Knagis/CommonMark.NET](https://github.com/Knagis/CommonMark.NET)

> Blockquote

---

`Inline code` with ticks

1. item1
2. item2

```f-sharp
code block
```


Links to other markup example pages:
[MarkDown], [Creole], [Textile]

